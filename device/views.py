import logging

from rest_framework.generics import ListCreateAPIView, RetrieveAPIView
from device.models import Device
from device.serializers import DeviceListSerializer, DeviceDetailSerializer

logger = logging.getLogger(__name__)


class DeviceListView(ListCreateAPIView):
    TAG = 'DeviceListView'
    queryset = Device.objects.all()
    serializer_class = DeviceListSerializer
    # permission_classes = [IsAuthenticated]
    # authentication_classes = [SessionAuthentication]

    def get(self, request, *args, **kwargs):
        logger.debug(".{}.get: request.data = {}".format(self.TAG, request.data))
        return super().get(request, *args, **kwargs)


class DeviceDetailView(RetrieveAPIView):
    queryset = Device.objects.all()
    serializer_class = DeviceDetailSerializer
    # permission_classes = [IsAuthenticated]
    # authentication_classes = [SessionAuthentication]
