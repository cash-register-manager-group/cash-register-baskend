from django.contrib import admin

from device.models import Device, Type, Locate

admin.site.register(Device)
admin.site.register(Type)
admin.site.register(Locate)
