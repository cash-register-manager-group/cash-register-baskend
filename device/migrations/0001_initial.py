# Generated by Django 3.2.5 on 2021-07-21 14:17

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Locate',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=200, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Type',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=200, null=True)),
                ('manufacturer', models.CharField(blank=True, max_length=200, null=True)),
                ('model', models.CharField(blank=True, max_length=200, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Device',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=200, null=True)),
                ('ip', models.CharField(blank=True, max_length=10, null=True)),
                ('description', models.TextField()),
                ('locate_id', models.ForeignKey(db_column='locate_id', on_delete=django.db.models.deletion.DO_NOTHING, to='device.locate')),
                ('type_id', models.ForeignKey(db_column='type_id', on_delete=django.db.models.deletion.DO_NOTHING, to='device.type')),
            ],
        ),
    ]
