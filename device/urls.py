from django.urls import path

from device.views import DeviceDetailView, DeviceListView

urlpatterns = [
    path('', DeviceListView.as_view()),
    path('<int:pk>/', DeviceDetailView.as_view()),
]
