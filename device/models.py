import os

from django.db import models


class Type(models.Model):
    name = models.CharField(max_length=200, blank=False, null=False, default='noname')
    manufacturer = models.CharField(max_length=200, blank=False, null=False, default='no manufacturer')
    model = models.CharField(max_length=200, blank=False, null=False, default='no model')
    image = models.ImageField(upload_to=os.path.join('media'), default='', blank=True)

    def __str__(self):
        return '{} ({} - {})'.format(self.name, self.manufacturer, self.model)


class Locate(models.Model):
    name = models.CharField(max_length=200, blank=False, null=False, default='noname')

    def __str__(self):
        return self.name


class Device(models.Model):
    name = models.CharField(max_length=200, blank=False, null=False, default='noname')
    locate_id = models.ForeignKey('Locate', models.DO_NOTHING, db_column='locate_id')
    type_id = models.ForeignKey('Type', models.DO_NOTHING, db_column='type_id')
    ip = models.CharField(max_length=20, blank=False, null=False, default='127.0.0.1')
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return '{} ({})'.format(self.name, self.ip)
