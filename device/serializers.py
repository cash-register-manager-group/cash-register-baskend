from rest_framework.serializers import ModelSerializer

from device import models


class DeviceListSerializer(ModelSerializer):
    class Meta:
        model = models.Device
        depth = 1
        fields = ['id', 'name', 'type_id']


class DeviceDetailSerializer(ModelSerializer):
    class Meta:
        depth = 1
        model = models.Device
        fields = '__all__'
