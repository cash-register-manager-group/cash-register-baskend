from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
                  path('admin/', admin.site.urls),
                  path('device/', include('device.urls')),
                  path('event/', include('event.urls')),
                  # path('token/', TokenObtainPairView.as_view()),
                  # path('token/refresh', TokenRefreshSlidingView.as_view()),
                  path('auth', include('rest_framework.urls')),
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
