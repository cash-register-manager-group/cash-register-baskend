from rest_framework.authentication import SessionAuthentication
from rest_framework.generics import ListCreateAPIView, ListAPIView, CreateAPIView
from rest_framework.permissions import IsAuthenticated

from event.models import Payment, ZReport, XReport
from event.serializers import PaymentSerializer, ZReportSerializer, XReportSerializer, XReportCreateSerializer


class PaymentView(ListCreateAPIView):
    queryset = Payment.objects.all()
    serializer_class = PaymentSerializer
    # permission_classes = [IsAuthenticated]
    # authentication_classes = [SessionAuthentication]


class XReportView(ListAPIView):
    queryset = XReport.objects.all()
    serializer_class = XReportSerializer
    # permission_classes = [IsAuthenticated]
    # authentication_classes = [SessionAuthentication]


class XReportCreateView(CreateAPIView):
    queryset = XReport.objects.all()
    serializer_class = XReportCreateSerializer
    # permission_classes = [IsAuthenticated]
    # authentication_classes = [SessionAuthentication]


class ZReportView(ListAPIView):
    queryset = ZReport.objects.all()
    serializer_class = ZReportSerializer
    # permission_classes = [IsAuthenticated]
    # authentication_classes = [SessionAuthentication]


class ZReportCreateView(CreateAPIView):
    queryset = ZReport.objects.all()
    serializer_class = XReportCreateSerializer
    # permission_classes = [IsAuthenticated]
    # authentication_classes = [SessionAuthentication]
