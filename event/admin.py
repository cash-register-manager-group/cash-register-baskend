from django.contrib import admin

from event.models import Payment, XReport, ZReport

admin.site.register(Payment)
admin.site.register(XReport)
admin.site.register(ZReport)
