from django.db import models


class Payment(models.Model):
    device_id = models.ForeignKey('device.Device', models.DO_NOTHING, db_column='device_id')
    date_create = models.DateTimeField(auto_now_add=True, blank=True)
    amount = models.IntegerField(default=0)
    good_name = models.CharField(max_length=200, default='NON')
    goods_code = models.IntegerField(default=0)
    error_code = models.IntegerField(default=0)
    error_msg = models.TextField(blank=True)

    def __str__(self):
        return '{}: amount={}, error={}'.format(self.device_id.name, self.amount, self.error_code)


class XReport(models.Model):
    device_id = models.ForeignKey('device.Device', models.DO_NOTHING, db_column='device_id')
    date_create = models.DateTimeField(auto_now_add=True, blank=True)
    error_code = models.IntegerField(default=0)
    error_msg = models.TextField(blank=True)

    def __str__(self):
        return '{}: error={}'.format(self.device_id.name, self.error_code)


class ZReport(models.Model):
    device_id = models.ForeignKey('device.Device', models.DO_NOTHING, db_column='device_id')
    date_create = models.DateTimeField(auto_now_add=True, blank=True)
    error_code = models.IntegerField(default=0)
    error_msg = models.TextField(blank=True)

    def __str__(self):
        return '{}: error={}'.format(self.device_id.name, self.error_code)
