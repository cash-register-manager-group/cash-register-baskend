import logging

import requests
from rest_framework.exceptions import ValidationError
from rest_framework.serializers import ModelSerializer

from device.models import Device
from event.models import Payment, ZReport, XReport

logger = logging.getLogger(__name__)


def connect_do_device(tag, validated_data, url):
    device_id: Device = validated_data['device_id']
    if not isinstance(device_id, Device):
        logger.error('{}.connect_do_device: device_id({}) is not Device type'.format(tag, device_id))
        raise TypeError('the device_id is not Device type')
    try:
        response = requests.post('http://{}/{}/'.format(device_id.ip, url))
        logger.info('{}.connect_do_device: response = {}'.format(tag, response))
    except requests.exceptions.RequestException as e:
        logger.warning('{}.create: connect error to {} - {}'.format(tag, device_id.ip, e.args[0]))
        raise ValidationError('Connect Error', 410)
    validated_data['error_code'] = response.status_code
    validated_data['error_msg'] = response.content
    return validated_data


class PaymentSerializer(ModelSerializer):
    class Meta:
        model = Payment
        fields = '__all__'


class XReportSerializer(ModelSerializer):
    TAG = 'XReportSerializer'
    URL = 'xreport'

    class Meta:
        model = XReport
        fields = '__all__'
        depth = 1


class XReportCreateSerializer(ModelSerializer):
    TAG = 'XReportCreateSerializer'
    URL = 'report_x'

    class Meta:
        model = XReport
        fields = '__all__'

    def create(self, validated_data):
        logger.debug('{}.create: validated_data='.format(self.TAG, validated_data))
        validated_data = connect_do_device(self.TAG, validated_data, self.URL)
        return super().create(validated_data)


class ZReportSerializer(ModelSerializer):
    TAG = 'ZReportSerializer'
    URL = 'report_z'

    class Meta:
        model = ZReport
        fields = '__all__'

    def create(self, validated_data):
        logger.debug('{}.create: ...'.format(self.TAG))
        validated_data = connect_do_device(self.TAG, validated_data, self.URL)
        return Payment.objects.create(**validated_data)
