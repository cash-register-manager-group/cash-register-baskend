from django.urls import path

from event.views import PaymentView, ZReportView, XReportView, XReportCreateView, ZReportCreateView

urlpatterns = [
    path('payment/', PaymentView.as_view()),
    path('xreport/', XReportView.as_view()),
    path('xreport/create/', XReportCreateView.as_view()),
    path('zreport/', ZReportView.as_view()),
    path('zreport/create/', ZReportCreateView.as_view()),
]
