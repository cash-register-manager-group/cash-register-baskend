from django.contrib.auth.models import AbstractUser


class UserModel(AbstractUser):
    first_name = None
    last_name = None

    class Meta:
        db_table = 'auth_user'
